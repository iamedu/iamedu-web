resource "docker_image" "nginx" {
  name = "nginx"

  build {
    path = "../../docker/nginx"
  }
}

resource "docker_image" "varnish" {
  name = "varnish"

  build {
    path = "../../docker/varnish"
  }
}

resource "docker_image" "webapp" {
  name = "webapp"

  build {
    path = "../../"
  }
}

resource "docker_network" "webapp_net" {
  name = "webapp_net"
}

resource "docker_container" "webapp" {
  name = "webapp"
  restart = "always"
  network_mode = "webapp_net"
  image = docker_image.webapp.latest
}

resource "docker_container" "varnish" {
  name  = "varnish"
  network_mode = "webapp_net"
  image = docker_image.varnish.latest

  depends_on = [docker_container.webapp]
}

resource "docker_container" "nginx" {
  name  = "nginx"
  network_mode = "webapp_net"
  image = docker_image.nginx.latest

  depends_on = [docker_container.varnish]

  ports {
    internal = 80
    external = 80
  }

  ports {
    internal = 443
    external = 443
  }

  provisioner "remote-exec" {
    inline = [
      "docker exec -it ${self.id} certbot --nginx -d iamedu.io -d iamedu.dev --non-interactive --agree-tos -m iamedu@gmail.com"
    ]

    connection {
      host = data.terraform_remote_state.docker_server.outputs.webapp_ip
      private_key = file("~/.ssh/id_rsa")
    }
  }
}

terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.16.0"
    }
  }
}

provider "docker" {
  host = "ssh://root@${data.terraform_remote_state.docker_server.outputs.webapp_ip}:22"
}

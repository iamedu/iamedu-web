terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.16.0"
    }
  }
}

variable "do_token" {}
variable "do_access_key" {}
variable "do_secret_key" {}
variable "do_region" {
  default = "nyc3"
}
variable "backend_space_name" {
  description="Space name for storing terraform state"
  default = "iamedu-terraform-state"
}
variable "ssh_fingerprint" {}

provider "digitalocean" {
  token             = var.do_token
  spaces_access_id  = var.do_access_key
  spaces_secret_key = var.do_secret_key
}

resource "digitalocean_droplet" "webapp" {
  name = "webapp"
  image = "ubuntu-21-10-x64"
  size = "s-1vcpu-2gb"
  region = var.do_region
  ssh_keys = [
    var.ssh_fingerprint
  ]

  provisioner "remote-exec" {
    inline = [
      "set -e",
      "until [ -f /var/lib/cloud/instance/boot-finished ]; do sleep 1; done",
      "apt-get update",
      "apt-get install -yq ca-certificates curl gnupg lsb-release",
      "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg",
      "echo \"deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\" | tee /etc/apt/sources.list.d/docker.list > /dev/null",
      "apt-get update",
      "apt-get install -yq docker-ce docker-ce-cli containerd.io"
    ]
    connection {
      host = self.ipv4_address
      private_key = file("~/.ssh/id_rsa")
    }
  }
}

output "webapp_ip" {
  value = digitalocean_droplet.webapp.ipv4_address
}

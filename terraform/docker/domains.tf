resource "digitalocean_record" "public_iamedu_io" {
  domain = "iamedu.io"
  type = "A"
  name = "@"
  value = digitalocean_droplet.webapp.ipv4_address
}

resource "digitalocean_record" "public_iamedu_dev" {
  domain = "iamedu.dev"
  type = "A"
  name = "@"
  value = digitalocean_droplet.webapp.ipv4_address
}

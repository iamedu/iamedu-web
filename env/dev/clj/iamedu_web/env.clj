(ns iamedu-web.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [iamedu-web.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[iamedu-web started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[iamedu-web has shut down successfully]=-"))
   :middleware wrap-dev})

(ns iamedu-web.routes.home
  (:require
   [iamedu-web.layout :as layout]
   [iamedu-web.middleware :as middleware]
   [iamedu-web.ui.pages.about :as about]
   [iamedu-web.ui.pages.notes :as notes]
   [ring.util.response]))

(defn home-page [request]
  (layout/render request (about/about-page)))

(defn notes-page [request]
  (layout/render request (notes/notes-page)))

(defn home-routes []
  [ "" 
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/" {:get home-page}]
   ["/notes" {:get notes-page}]])


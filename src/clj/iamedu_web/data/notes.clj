(ns iamedu-web.data.notes)

(def notes
  [{:key "overengineering-101"
    :title "Overengineering 101"
    :subtitle "Don't do it because it's necessary, do it because it's fun."
    :blurb "Let's dive into the why and how this site works."
    :image "https://cdn.iamedu.io/public/img/thoughts/overengineering-101/site-architecture.drawio.png"}])

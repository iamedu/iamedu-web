(ns iamedu-web.ui.markdown
  (:require [clojure.java.io :as io]
            [markdown-to-hiccup.core :as mth]))


(defn resource-to-hiccup [resource-path]
  (-> resource-path
      (io/resource)
      (slurp)
      (mth/md->hiccup)
      (mth/component)))

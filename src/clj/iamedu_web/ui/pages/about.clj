(ns iamedu-web.ui.pages.about
  (:require [iamedu-web.config :as config]
            [iamedu-web.ui.markdown :as md])
  (:use hiccup.element
        hiccup.page
        iamedu-web.ui.components))

(defn about-page []
  (html5 [:html
          (html-head config/title-prefix "About Me"
                     :css-sheets (config/css-sheets)
                     :raw-resources config/raw-resources)
          [:body
           (top-bar :about-me config/sections)
           (body-container
             (center-content
               [:h2.title.is-2 "About me"]
               (pic-circle
                 (config/cdn-resource "img/iamedu.jpg")))
             [:div.section
              [:div.columns
               [:div.column.is-half.is-offset-one-quarter
                [:div.content
                 (md/resource-to-hiccup "content/about.md")]]]])
           (footer "about.clj"
                   :gitlab-prefix config/gitlab-prefix
                   :current-year (config/current-year))]]))

(ns iamedu-web.ui.pages.notes
  (:require [iamedu-web.config :as config]
            [iamedu-web.data.notes :as data])
  (:use hiccup.element
        hiccup.page
        iamedu-web.ui.components))

(defn note-blurb [{:keys [title subtitle blurb image]}]
  [:div.tile.is-parent
   [:article.tile.is-child.box
    [:p.title title]
    [:p.subtitle subtitle]
    [:div.columns.is-centered
     [:div.column.is-half
      [:figure.image.is-1by1
       [:img {:src image}]]]]
    [:p blurb]]])

(defn notes-section [notes]
  (for [note-rows (partition-all 2 notes)]
    [:div.tile.is-ancestor
     (for [note note-rows]
       (note-blurb note))]))

(defn notes-page []
  (html5 [:html
          (html-head config/title-prefix "My notes"
                     :css-sheets (config/css-sheets)
                     :raw-resources config/raw-resources)
          [:body
           (top-bar :notes config/sections)
           (body-container
             (notes-section data/notes))
           (footer "notes.clj"
                   :gitlab-prefix config/gitlab-prefix
                   :current-year (config/current-year))]]))

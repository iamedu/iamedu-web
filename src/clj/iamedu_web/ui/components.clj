(ns iamedu-web.ui.components
  (:require [clojure.java.io :as io]
            [iamedu-web.config :as config])
  (:use hiccup.page))

(defn read-raw-resource [resource-file]
  (-> resource-file
      (io/resource)
      (slurp)))

(defn html-head [title-prefix title & {:keys [css-sheets raw-resources]}]
  [:head
   [:title (str title-prefix title)]
   [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
   [:link {:rel "icon" :type "image/x-icon" :href (config/cdn-resource "favicon.ico")}]
   [:link {:rel "apple-touch-icon" :href (config/cdn-resource "apple-touch-icon.png")}]
   (apply include-css css-sheets)
   (for [raw-resource raw-resources]
     (read-raw-resource raw-resource))])

(defn top-bar [selected sections]
  [:div#top-bar.section
   [:div.container
    [:nav.level
     [:div.level-left
      [:h1.title"@iamedu"]]
     [:div.tabs.level-right
      [:ul
       (for [{:keys [key name url]} sections]
         [:li (if (= selected key) {:class "is-active"})
          [:a {:href url} name]])]]]]])

(defn footer [source-link & {:keys [current-year gitlab-prefix]}]
  (let [full-link (str gitlab-prefix source-link)]
    [:footer.footer
     [:div.content.has-text-centered
      [:p
       "Site by "
       [:strong "Eduardo Diaz"]
       " "
       current-year
       ". "
       "Look at the source for this page "
       [:a {:href full-link} "here!"]]]]))

(defn pic-circle [url]
  [:figure.image.is-128x128
   [:img.is-rounded {:src url :width 128 :height 128}]])

(defn center-content [& content]
  [:nav.level
   [:div.level-item
    [:div.content content]]])

(defn body-container [& content]
  [:div.section
   [:div.container content]])

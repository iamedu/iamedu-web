(ns iamedu-web.config
  (:import [java.time LocalDate])
  (:require
    [cprop.core :refer [load-config]]
    [cprop.source :as source]
    [mount.core :refer [args defstate]]))

(defstate env
  :start
  (load-config
    :merge
    [(args)
     (source/from-system-props)
     (source/from-env)]))

(defn cdn-resource [path]
  (let [prefix (-> env :cdn-prefix)]
    (str prefix path)))

(defn current-year []
  (-> (LocalDate/now)
      (.getYear)))

(defn css-sheets []
  ["https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css"
   (cdn-resource "css/main.css")])

(def raw-resources
  ["html/analytics.html"])

(def gitlab-prefix
  "https://gitlab.com/iamedu/iamedu-web/-/tree/master/src/clj/iamedu_web/ui/pages/")

(def title-prefix
  "@iamedu | ")

(def sections
  [{:key :about-me
    :url "/"
    :name "About me"}
   {:key :notes
    :url "/notes"
    :name "Notes"}])
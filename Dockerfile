FROM clojure:openjdk-8-lein-2.9.8-buster as builder

RUN mkdir /app
WORKDIR /app
COPY . .
RUN lein uberjar

FROM openjdk:17

RUN mkdir /app
COPY --from=builder /app/target/uberjar/iamedu-web.jar /app/app.jar
EXPOSE 3000

CMD ["java", "-jar", "/app/app.jar"]

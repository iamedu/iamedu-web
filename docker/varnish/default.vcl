vcl 4.1;

backend default {
    .host = "webapp";
    .port = "3000";
}

acl purge {
    "webapp";
    "localhost";
    "127.0.0.1";
    "::1";
}

sub vcl_backend_response {
    unset beresp.http.Set-Cookie;
}

sub vcl_recv {
    unset req.http.proxy;
    unset req.http.x-cache;

    if (req.method == "PURGE") {
        if (!client.ip ~ purge) {
            return(synth(405,"Not allowed."));
        }
        return (purge);
    }

    return(hash);
}

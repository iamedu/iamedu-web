I’m **Eduardo Diaz**. Currently working as a Software Engineer @ Meta.

My most current experience is in Android, but I’ve spent a good deal of time working on web solutions.

I love learning new things, in the future I’m interested in building AR or hardware.